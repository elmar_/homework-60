import React from 'react';
import moment from 'moment';
import './MessageBlock.css';

const MessageBlock = ({message}) => {
    return (
        <div className="MessageBlock">
            <div className="topMessage">
                <h4 className="nonM"><span>Author:</span> {message.author}</h4>
                <p className="nonM">Date: {moment(message.datetime).format('MMMM Do YYYY, h:mm:ss a')}</p>
            </div>
            <p>{message.message}</p>
        </div>
    );
};

export default MessageBlock;