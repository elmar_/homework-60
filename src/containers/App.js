import React, {useState, useEffect} from 'react';
import './App.css';
import MessageBlock from "../components/MessageBlock/MessageBlock";

const App = () => {

    const [message, setMessage] = useState({});
    const [urlMessages, setUrlMessages] = useState([]);
    const [startInterval, setStartInterval] = useState(false);
    const [lateDate, setLateDate] = useState("");

    useEffect(() => {
        const url = 'http://146.185.154.90:8000/messages';
        const gettingMessages = async () => {
            const response = await fetch(url);
            const data = await response.json();
            setLateDate(data[data.length - 1].datetime);
            setStartInterval(true);
            setUrlMessages(data.reverse());
        };
        gettingMessages().catch(e => alert(e));

    }, []);


    useEffect(() => {
        setInterval(async () => {
            const url = 'http://146.185.154.90:8000/messages?datetime=' + lateDate;
            const response = await fetch(url);
            const data = await response.json();
            if (data.length > 0) {
                const reverseData = data.reverse();
                setLateDate(reverseData[0].datetime);
                const urlMessagesCopy = [...reverseData, ...urlMessages];
                setUrlMessages(urlMessagesCopy);
            }
        }, 3000)
    }, [startInterval])


    const getMessage = e => {
        const messageCopy = {};
        messageCopy.message = e.target.value;
        messageCopy.author = message.author;

        setMessage(messageCopy);
    };

    const getAuthor = e => {
        const messageCopy = {};
        messageCopy.message = message.message;
        messageCopy.author = e.target.value;

        setMessage(messageCopy);
    };

    const postMessage = async () => {
        const url = 'http://146.185.154.90:8000/messages';

        const data = new URLSearchParams();
        data.set('message', message.message);
        data.set('author', message.author);

        const response = await fetch(url, {
            method: 'post',
            body: data,
        });
    };

    return (
        <div className="App">
            <p><input type="text" className="input-author" onChange={e => getAuthor(e)} /> Автор</p>
            <p><input type="text" className="input-text" id="messageValue" onChange={e => getMessage(e)} /> Сообщение</p>
            <button className="btn" onClick={postMessage}>Send</button>
            {urlMessages.map(mes => <MessageBlock message={mes} />)}
        </div>
    );
};

export default App;